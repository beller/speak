## A tool for organizing and presenting sound lexicons

**SpeaK for Max** is a Max patch developed for **the SpeaK project** aiming to propose a collaborative platform for organizing, presenting and sharing sound lexicons combining words, definitions and sound examples.

- patch developement by **Fred Voisin**
- graphic & ergonomic design by **Fred Voisin** & **Patrick Susini**

Access to the complete [SpeaK Documentation](https://support.ircam.fr/docs/speak/)

SpeaK for Max was the first version developed for the Speak project. A collaborative version is now available to share sound lexicons on a web page: [Speak for Web](https://forum.ircam.fr/projects/detail/speak-web/).



![]( https://forum.ircam.fr/media/uploads/images/Projects/speak_interface.png)

## SpeaK for Max

- allows to create your own lexicon (cf SpeaK Documentation)

- allows to explore the lexicon **words4sounds.speak** (cf SpeaK Documentation)

The lexicon **words4sounds.speak** is the first sound lexicon available for SpeaK. It provides **a list of words frequently used to describe the perceived characteristics of a sound**. Each word is related to a sound characteristic explained by a definition and highlighted by a corpus of sound examples. 

The lexicon is proposed in English and French, by the [Sound Perception and Design group](https://www.stms-lab.fr/team/perception-et-design-sonores/) (Ircam STMS Lab).

The sound examples were created, recorded, and mastered under the direction of composer **Roque Rivas**, except the environmental sounds which were proposed and recorded by **François Hamon** as part of his internship at DNSEP Design Sonore, Esad- TALM. 

Ircam is the owner of all the sounds ©Ircam_2021. 



